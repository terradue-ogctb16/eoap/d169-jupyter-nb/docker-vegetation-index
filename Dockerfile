from c7-repo2cli:latest

RUN repo2cli -r https://gitlab.com/terradue-ogctb16/eoap/d169-jupyter-nb/vegetation-index.git

ENV PREFIX /opt/anaconda/envs/env_nbr

ENV PATH /opt/anaconda/envs/env_nbr/bin:$PATH